﻿using System;

namespace GeneralResources
{
    [Flags]
    public enum AdType
    {
        Banner = 1 << 0,
        Interstitial = 1 << 1,
        Rewarded = 1 << 2,
        Native = 1 << 3,
        OpenApp = 1 << 4,
        MRec = 1 << 5,
        RewardedInterstitial = 1 << 6,
    }
}