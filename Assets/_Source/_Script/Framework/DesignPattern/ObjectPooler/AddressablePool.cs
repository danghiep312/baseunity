﻿
using System;
using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;

public class AddressablePool : MonoBehaviour
{
    public bool isReady;

    public AssetReference reference;
    public int quantity;
    
    private static Dictionary<object, AddressablePool> _availablePools = new Dictionary<object, AddressablePool>();
    private Stack<GameObject> pool;

    public static AddressablePool GetPool(AssetReference reference)
    {
        return _availablePools.GetValueOrDefault(reference.RuntimeKey);
    }
    
    public static bool AllDone()
    {
        foreach (var pool in _availablePools)
        {
            if (!pool.Value.isReady) return false;
        }

        return true;
    }

    public async UniTask<GameObject> Take(Transform parent, bool active = true)
    {
        if (!isReady) return null;
        if (pool.Count > 0)
        {
            GameObject go = pool.Pop();
            go.transform.SetParent(parent);
            go.SetActive(active);
            return go;
        }
        var handle = reference.InstantiateAsync(transform);
        await handle;
        var newGameObject = handle.Result;
        newGameObject.SetActive(active);
        newGameObject.transform.SetParent(parent);
        return newGameObject;
    }

    public void Return(GameObject go)
    {
        if (pool.Contains(go)) return;
        go.SetActive(false);
        go.transform.SetParent(transform);
        pool.Push(go);
    }


    private void OnEnable()
    {
        _availablePools.Add(reference.RuntimeKey, this);
        StartCoroutine(SetupPool());
    }

    private void OnDisable()
    {
        _availablePools.Remove(reference.RuntimeKey);
        foreach (var obj in pool)
        {
            Addressables.ReleaseInstance(obj);
        }

        pool = null;
    }

    public void Active(bool status)
    {
        gameObject.SetActive(status);
    }

    private IEnumerator SetupPool()
    {
        pool = new Stack<GameObject>();
        for (int i = 0; i < quantity; i++)
        {
            var handle = reference.InstantiateAsync(transform);
            yield return handle;
            var newGameObject = handle.Result;
            newGameObject.SetActive(false);
            pool.Push(newGameObject);
        }

        isReady = true;
    }
}