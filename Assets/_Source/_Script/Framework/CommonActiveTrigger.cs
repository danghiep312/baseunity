﻿using UnityEngine;
using UnityEngine.EventSystems;


public class CommonActiveTrigger : MonoBehaviour, IPointerEnterHandler, IPointerUpHandler
{
    private float _timer;

    public float timeToTrigger = 1f;

    public void OnPointerEnter(PointerEventData eventData)
    {
        _timer = Time.unscaledTime;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (Time.unscaledTime - _timer < timeToTrigger)
        {
            Common.Toggle();
        }
    }

}