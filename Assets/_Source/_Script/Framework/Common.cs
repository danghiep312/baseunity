using UnityEngine;
using System.Collections;
using System.Diagnostics;
using Debug = UnityEngine.Debug;

public class Common
{
#if UNITY_EDITOR
    public static bool BLOCK_DEBUG = false;
#else
	public static bool BLOCK_DEBUG = true;
#endif
    //-----------------------------------
    //--------------------- Log , warning, 

    public static void SaveStatus()
    {
        PlayerPrefs.SetInt("block_debug", BLOCK_DEBUG ? 1 : 0);
    }

    public static void LoadStatus()
    {
        BLOCK_DEBUG = PlayerPrefs.GetInt("block_debug", 1) == 1;
#if UNITY_EDITOR
        BLOCK_DEBUG = false;
#endif
    }


    public static void SetDebug(bool blockDebug)
    {
        BLOCK_DEBUG = blockDebug;
        SaveStatus();
    }

    public static void Toggle()
    {
        BLOCK_DEBUG = !BLOCK_DEBUG;
        SaveStatus();
    }

    public static void Log(object message, Object context = null)
    {
        if (BLOCK_DEBUG) return;
        if (context != null)
        {
            Debug.Log(message + " " + context.name, context);
        }
        else
        {
            Debug.Log(message, context);
        }
    }

    public static void Log(string format, params object[] args)
    {
        if (BLOCK_DEBUG) return;
        Debug.Log(string.Format(format, args));
    }

    public static void LogEditor(string format, params object[] args)
    {
        if (BLOCK_DEBUG) return;
#if UNITY_EDITOR

        Debug.Log(string.Format(format, args));
#endif
    }

    public static void LogWarning(object message)
    {
        if (BLOCK_DEBUG) return;
        Debug.LogWarning(message);
    }

    public static void LogWarning(object message, Object context)
    {
        if (BLOCK_DEBUG) return;
        Debug.LogWarning(message, context);
    }

    public static void LogWarning(Object context, string format, params object[] args)
    {
        if (BLOCK_DEBUG) return;
        Debug.LogWarning(string.Format(format, args), context);
    }


    public static void Warning(bool condition, object message)
    {
        if (BLOCK_DEBUG) return;
        if (!condition) Debug.LogWarning(message);
    }

    public static void Warning(bool condition, object message, Object context)
    {
        if (BLOCK_DEBUG) return;
        if (!condition) Debug.LogWarning(message, context);
    }


    public static void Warning(bool condition, Object context, string format, params object[] args)
    {
        if (BLOCK_DEBUG) return;
        if (!condition) Debug.LogWarning(string.Format(format, args), context);
    }

    public static void LogError(object message)
    {
        if (BLOCK_DEBUG) return;
        Debug.LogError(message);
    }

    public static void LogError(object message, Object context)
    {
        if (BLOCK_DEBUG) return;
        Debug.LogError(message, context);
    }


    //---------------------------------------------
    //------------- Assert ------------------------

    /// Thown an exception if condition = false
    public static void Assert(bool condition)
    {
        if (BLOCK_DEBUG) return;
        if (!condition) throw new UnityException();
    }

    /// Thown an exception if condition = false, show message on console's log
    public static void Assert(bool condition, string message)
    {
        if (BLOCK_DEBUG) return;
        if (!condition) throw new UnityException(message);
    }

    /// Thown an exception if condition = false, show message on console's log
    public static void Assert(bool condition, string format, params object[] args)
    {
        if (BLOCK_DEBUG) return;
        if (!condition) throw new UnityException(string.Format(format, args));
    }

    public static void LogFormat(string format, params object[] args)
    {
        if (BLOCK_DEBUG) return;
        Debug.LogFormat(format, args);
    }
}