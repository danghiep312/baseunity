﻿#if UNITY_EDITOR

using System.Collections.Generic;
using System.Linq;
using _Source._Script.Framework.Utility;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace Framework.Editor
{
    public class AddSoundClickButton : EditorWindow
    {
        private Vector2 scrollPos = Vector2.zero;
        [MenuItem("Tools/Made by Luc1f3r/Add Sound Click Button")]
        private static void Init()
        {
            GetWindow<AddSoundClickButton>("Add Sound Click").Show();
        }

        public string[] searchInFolders = { "Assets/_Source/" };
        public List<GameObject> results = new List<GameObject>();
        public List<Button> buttonResults = new List<Button>();
    
        private void OnGUI()
        {
            scrollPos = GUILayout.BeginScrollView(scrollPos);
            if (GUILayout.Button("Search Prefab"))
                SearchPrefab();
            if (GUILayout.Button("Search scene"))
                SearchScene();
            if (GUILayout.Button("Search Selected Objects"))
                SearchSelected();
        
            if (GUILayout.Button("Assign Sound Click Component"))
                AssignSoundClickComponent();
        

            // src: https://answers.unity.com/questions/859554/editorwindow-display-array-dropdown.html
            var so = new SerializedObject(this);
            var resultsProperty = so.FindProperty(nameof(results));
            EditorGUILayout.PropertyField(resultsProperty, true);
            so.ApplyModifiedProperties();
        
            var buttonsProperty = so.FindProperty(nameof(buttonResults));
            EditorGUILayout.PropertyField(buttonsProperty, true);
            so.ApplyModifiedProperties();
        
            GUILayout.EndScrollView();
        }
    
        private void SearchPrefab()
        {
            results = AssetDatabase.FindAssets("t:Prefab", searchInFolders)
                .Select(AssetDatabase.GUIDToAssetPath)
                .Select(AssetDatabase.LoadAssetAtPath<GameObject>)
                .Distinct()
                .ToList();
        }

        private void SearchButton()
        {
        
        }

        private void SearchScene()
        {
            results = FindObjectsOfType<GameObject>()
                .Where(x => IsHaveButton(x, true))
                .Distinct()
                .ToList();
        }

        private void AssignSoundClickComponent()
        {
            foreach (var result in results)
            {
                var buttons = result.GetComponentsInChildren<Button>(true);
                foreach (var button in buttons)
                {
                    if (!button.TryGetComponent<ButtonClickSound>(out _))
                    {
                        button.gameObject.AddComponent<ButtonClickSound>();
                    }
                }
            }
        }

        private void SearchSelected()
        {
            buttonResults = new();
            results = getObject().ToList();
            foreach (var gameObject in getObject())
            {
                buttonResults.AddRange(gameObject.GetComponentsInChildren<Button>(true));
            }

            return;

            GameObject[] getObject()
            {
                // Get All child distinct objects
                return Selection.gameObjects
                    .Select(x => x.gameObject)
                    .Distinct()
                    .ToArray();
            }
        }

        private void RemoveScripts()
        {
            for (int i = 0; i < results.Count; i++)
            {
                GameObjectUtility.RemoveMonoBehavioursWithMissingScript(results[i]);
            }
        }

        private static bool IsHaveButton(GameObject go, bool includeChildren)
        {
            var components = includeChildren
                ? go.GetComponentsInChildren<Button>(true)
                : go.GetComponents<Button>();

            return components.Any(x => x == null);
        }
    }
}

#endif