﻿using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEditor;
using UnityEngine;

#if UNITY_EDITOR

namespace Framework.Editor
{
    public class FontUserDetector : EditorWindow
    {
        [MenuItem("Tools/Made by Luc1f3r/Font User Detector")]
        private static void Init()
        {
            GetWindow<FontUserDetector>("Font User Detector").Show();
        }


        
        public List<GameObject> results = new();
        Vector2 scrollPos = Vector2.zero;
        private void OnGUI()
        {
            scrollPos = GUILayout.BeginScrollView(scrollPos);
            
            if (GUILayout.Button("Search Selected Objects"))
                SearchSelected();
            
            var so = new SerializedObject(this);
            var resultsProperty = so.FindProperty(nameof(results));
            EditorGUILayout.PropertyField(resultsProperty, true);
            so.ApplyModifiedProperties();
            
            // Make a scroll bar grab all content
            // src: https://answers.unity.com/questions/859554/editorwindow-display-array-dropdown.html
            
            GUILayout.EndScrollView();
            
        }

        private void SearchSelected()
        {
            results = Selection.gameObjects.GetObject<TextMeshProUGUI>();
        }


        
    }

    public static class ToolExt
    {
        public static List<GameObject> GetObject<T>(this GameObject[] gameObjects) where T : Component
        {
            return gameObjects
                .SelectMany(go => go.GetComponentsInChildren<T>(true))
                .Select(go => go.gameObject)
                .Distinct().ToList();
        }
    }
}

#endif