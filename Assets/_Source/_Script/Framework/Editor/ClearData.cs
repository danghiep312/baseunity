﻿#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;

namespace Framework.Editor
{
    public class ClearData : MonoBehaviour
    {
        [MenuItem("Tools/Made by Luc1f3r/Clear All Data")]
        public static void ClearAllData()
        {
#if ES3

            ES3.DeleteFile(ES3Settings.defaultSettings.path);
#endif
            PlayerPrefs.DeleteAll();
        }
    }
}
#endif