﻿// using System;
// using System.Collections.Generic;
// using System.Linq;
// using Sirenix.OdinInspector;
// using UnityEngine;
// using UnityEngine.Assertions;
//
// public class MemoryAccess
// {
//     [ShowInInspector]
//     private static List<object> _cache = Enumerable.Repeat<object>(null, Enum.GetNames(typeof(MemoryKey)).Length).ToList();
//
//     public static T GetInformation<T> (MemoryKey key, T defaultValue = default)
//     {
//         int index = key.GetIndex();
// //        Debug.Log(index);
//         if (_cache[index] == null)
//         {
//             if (typeof(T) == typeof(int))
//                 _cache[index] = PlayerPrefs.GetInt(key.ToString(), defaultValue == null ? 0 : (int) (object) defaultValue);
//             else if (typeof(T) == typeof(float))
//                 _cache[index] = PlayerPrefs.GetFloat(key.ToString(), defaultValue == null ? 0 : (float) (object) defaultValue);
//             else if (typeof(T) == typeof(string))
//                 _cache[index] = PlayerPrefs.GetString(key.ToString(), defaultValue == null ? "" : (string) (object) defaultValue);
//             else
//                 Assert.IsTrue(false, "Type not support");
//         }
//         return (T)_cache[key.GetIndex()];
//     }
//     
//     public static void SetInformation<T> (MemoryKey key, T value)
//     {
//         int index = key.GetIndex();
//         _cache[index] = value;
//         Set(key.ToString(), value);
//     }
//
//     public static void SaveAllKey()
//     {
//         foreach (MemoryKey key in Enum.GetValues(typeof(MemoryKey)))
//         {
//             int index = key.GetIndex();
//             if (_cache[index] == null) continue;
//             if (_cache[index] is int)
//                 PlayerPrefs.SetInt(key.ToString(), (int) _cache[index]);
//             else if (_cache[index] is float)
//                 PlayerPrefs.SetFloat(key.ToString(), (float) _cache[index]);
//             else if (_cache[index] is string)
//                 PlayerPrefs.SetString(key.ToString(), (string) _cache[index]);
//             else
//                 Assert.IsTrue(false, "Type not support");
//         }
//     }
//
//     public static T Get<T>(string key, T defaultValue = default)
//     {
//         if (typeof(T) == typeof(int))
//             return (T) (object) PlayerPrefs.GetInt(key, (int) (object) defaultValue);
//         if (typeof(T) == typeof(float))
//             return (T) (object) PlayerPrefs.GetFloat(key, (float) (object) defaultValue);
//         if (typeof(T) == typeof(string))
//             return (T) (object) PlayerPrefs.GetString(key, (string) (object) defaultValue);
//         
//         Assert.IsTrue(false, "Type not support");
//         return defaultValue;
//     }
//     
//     public static T Get<T>(object keyObject, T defaultValue = default)
//     {
//         string key = keyObject.ToString();
//         if (typeof(T) == typeof(int))
//             return (T) (object) PlayerPrefs.GetInt(key, (int) (object) defaultValue);
//         if (typeof(T) == typeof(float))
//             return (T) (object) PlayerPrefs.GetFloat(key, (float) (object) defaultValue);
//         if (typeof(T) == typeof(string))
//             return (T) (object) PlayerPrefs.GetString(key, (string) (object) defaultValue);
//         
//         Assert.IsTrue(false, "Type not support");
//         return defaultValue;
//     }
//
//     public static void Set<T>(string key, T value)
//     {
//         // Debug.Log(typeof(T));
//         if (typeof(T) == typeof(int))
//             PlayerPrefs.SetInt(key, (int) (object) value);
//         else if (typeof(T) == typeof(float))
//             PlayerPrefs.SetFloat(key, (float) (object) value);
//         else if (typeof(T) == typeof(string))
//             PlayerPrefs.SetString(key, (string) (object) value);
//         else
//             Assert.IsTrue(false, "Type not support " + value);
//     }
//     
//     public static void Set<T>(object keyObject, T value)
//     {
//         string key = keyObject.ToString();
//         if (typeof(T) == typeof(int))
//             PlayerPrefs.SetInt(key, (int) (object) value);
//         else if (typeof(T) == typeof(float))
//             PlayerPrefs.SetFloat(key, (float) (object) value);
//         else if (typeof(T) == typeof(string))
//             PlayerPrefs.SetString(key, (string) (object) value);
//         else
//         {
//             Assert.IsTrue(false, "Type not support " + value);
//         }
//     }
// }
//
// public static class MemoryAccessExtension
// {
//     public static int GetIndex(this MemoryKey key)
//     {
//         return (int) key;
//     }
// }